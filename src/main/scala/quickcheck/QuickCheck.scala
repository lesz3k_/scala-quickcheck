package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = oneOf(
    const(empty),
    for {
      a <- arbitrary[A]
      b <- oneOf(const(empty), genHeap)
    } yield insert(a, b))

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

    property("link") = forAll { (a: Int, b: Int) =>
      if (a > b) {
        val h1 = insert(b, insert(a, empty))
        findMin(h1) == b
        findMin(deleteMin(h1)) == a
      } else {
        val h1 = insert(a, insert(b, empty))
        findMin(h1) == a
        findMin(deleteMin(h1)) == b
      }
    }

  property("deleteMin") = forAll { (h: H) =>
    if (isEmpty(h)) Prop.throws(classOf[NoSuchElementException])(deleteMin(h))
    else {
      val m = findMin(h)
      val delHeap = deleteMin(h)
      insert(m, delHeap) == h
    }
  }

}
